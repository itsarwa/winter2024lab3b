public class Fish{
	
	public String species;
	public int lengthInInches;
	public boolean isSaltWater;
	
	public Fish (String s, int l, boolean isSalt){
		
		species=s;
		lengthInInches=l;
		isSaltWater=isSalt;
	}
	
	public String nameSpecies(){
		return "This is a " + species + "!";
		
	}
	
	public boolean canSwimInSalt(){
		
		if("Goldfish".equals(species)){
			return false;
			
		}else{
			return true;
		}
	}
		

}