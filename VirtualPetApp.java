import java.util.Scanner;

public class VirtualPetApp{
	
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		
		// Fish f1= new Fish ("GoldFish", 6, false);
		// Fish f2= new Fish ("Clownfish", 3, true);
		// Fish f3= new Fish ("Tuna", 7, true);
		
		// System.out.println( f1.nameSpecies());
		// System.out.println( f1.canSwimInSalt());
		
		Fish[] shoalOfFish= new Fish[4];
			
			for(int i=0; i<shoalOfFish.length; i++){
				
				System.out.println("What is the species of your fish?");
				String species= scanner.nextLine();
				
				System.out.println("What is the length (inches) of your fish?");
				int lengthInInches = Integer.parseInt(scanner.nextLine());
				
				System.out.println("Can your fish swim in salt water?");
				
				String userInput = scanner.nextLine();
				boolean isSaltWater = userInput.equalsIgnoreCase("yes");

				shoalOfFish[i] = new Fish(species, lengthInInches, isSaltWater);
			
		}
			//printing the 3 fields of the last animal in array
			System.out.println("The last fish's species: " + shoalOfFish[3].species);
			System.out.println("Length (inches): " + shoalOfFish[3].lengthInInches);
			System.out.println( "Can swim in salt water: " + shoalOfFish[3].isSaltWater);
	
			//calling the 2 instance methods from Fish.java on first animal of array
			System.out.println(shoalOfFish[0].nameSpecies());
			System.out.println(shoalOfFish[0].canSwimInSalt());
	}
}